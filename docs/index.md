## [Paul Gibson](https://resume.pgibson.us)  [![tst](https://resume.pgibson.us/img/icons8-linkedin-16.png)](https://www.linkedin.com/in/paulgibson/)


### Summary
Experience in:

- building, operating, and evolving commercial SaaS systems
- utilizing cloud technologies, particularly Amazon Web Services
- modernizing mature software systems and managing technical debt
- characterizing cost of ownership and leading projects to improve it
- understanding and applying industry best practices
- filling the void between Dev and Ops and leading others to do the same

Employment in Silicon Valley has an aspect of investing (via equity).  To that end I have gained this experience and perspective:

- worked for 6 pre-public companies
- achieved 7 positive exit milestones so far (2 IPOs, 3 acquisitions, 2 private equity purchases)
- developed an understanding about what matters for growing a company and what doesn't

### Experience
**Systems and Infrastructure Architect, [Bill.com](https://www.bill.com/)**  
September 2019 - present, Palo Alto, CA  

Working on the cloud migration.

**Principal Systems Architect, [Financial Engines](https://www.crunchbase.com/organization/financial-engines) aka [Edelman Financial Engines](https://www.edelmanfinancialengines.com/)**  
September 2014 - September 2019, Sunnyvale, CA  
Led Financial Engine's lift-and-shift journey to AWS:

- Completed all-in on AWS and [full colo exit](https://resume.pgibson.us/img/empty-colo-cage.jpg) for dev, test, DR, and production as of September 2019
- Partnered with FE's Analytics Office to migrate from an on-prem Netezza data warehouse to Redshift (completed May 2018)
- Rehosted production Linux compute to EC2 (completed December 2017)
- Migrated DR from colocation to a [hybrid model using 1 colocation rack and the rest in AWS](https://medium.com/financial-engines-techblog/disaster-recovery-using-hybrid-cloud-e32e8effa9c4) (completed 2016)
- Adopted higher-layer managed services such as ElastiCache in favor of run-your-own Redis
- partnered with FE's Tech Office on a parallel journey of [Cloud Native transformation](https://aws.amazon.com/solutions/case-studies/financial-engines/)
- over the 5-year journey reduced total colocation footprint from ~25 racks to zero

**SaaS Infrastructure Architect, [Intuit Inc.](https://www.intuit.com/)**  
July 2009 - September 2014, Mountain View, CA  
Joined Intuit as part of the [PayCycle acquistion](https://investors.intuit.com/press-releases/press-release-details/2009/Intuit-Completes-Acquisition-of-PayCycle/default.aspx).  

Worked primarily in Online Payroll and the broader set of Small Business SaaS offerings including Quickbooks Online.  

Started the cloud migration by leading the design and led the implementation of a hybrid cloud environment:

- spanning a lab on Intuit's Mountain View campus and AWS us-west-1 NorCal
- using AWS Direct Connect 
- which allowed for seamless expansion of the lab into the cloud
- the experiment with pre-production helped inform how the same approach could be used for DR and production footprints

**Principal Engineer - Infrastructure, [PayCycle, Inc.](https://www.crunchbase.com/organization/paycycle)**  
October 2005 - July 2009, Palo Alto, CA  
Worked on:

- performance and scalability work for a Java-based SaaS application
- Disaster Recovery architecture, design, implementation and production activation for ~2 weeks annually
- appserver migration from WebLogic to Glassfish to enable >100k/year in license expense savings with no loss of functionality
- various sub-system upgrades (Quartz, slf4j/logback)
- migration of app- and web-tier hosting from bare metal to virtualized
- MFA authentication design

**Engineering Manageer - Infrastructure, [Calyso Technology](https://www.crunchbase.com/organization/calypso-technology)**  
June 2004 - October 2005, San Francisco, CA  
Led a small team of engineers responsible for:

- the Calyso Data Server
- the Calyso Event Server
- the data model
- integrations with 3rd party software partners such as [DataSynapse](https://www.crunchbase.com/organization/datasynapse)

**Staff Engineer, [Siebel Systems](https://www.crunchbase.com/organization/siebel)**  
June 2003 - May 2004, San Mateo, CA  
Joined Siebel as part of the [BoldFish acquisition](https://www.computerworld.com.au/article/66787/siebel_acquires_boldfish/).  
Ensured a smooth transition of the BoldFish technology into the Siebel Email Marketing solution.

**Architect, [BoldFish](https://www.crunchbase.com/organization/boldfish)**  
May 2000 - June 2003, Santa Clara, CA  
Led a small team to implement the BoldFish Express Network:

- a cloud-based high-volume email sending service used by customers for email marketing
- implemented seamless integration with the on-premise BoldFish Express Server that integrated with customer databases (typically CRM)
- utilized a scale-out design for Express Network using JMS and the BoldFish Sending Engine, a high-volume egress MTA


**System Architect, [wine.com](https://www.crunchbase.com/organization/wine-com)**  
April 1998 - April 2000, Palo Alto, CA  
Joined as employee ~35 when the company was still Virtual Vineyards.  Led these projects:

- move from Berkeley DB to Oracle 8 for the backend database
- move from on-premise hosting over a T1 to a commercial colocation facility
- move from virtualvineyards.com domain to wine.com
- move from Perl/CGI backend application codebase to Java Servlet/JSP running on Apache JServ
- move to application tier front-ended by f5 BigIP load balancers
- move from a monolithic application tier on a single Sun E4500 mid-range machine to scale-out on SPARC 5 machines
- implemented Akamai as a CDN solution

Over this time wine.com grew to a $65M annual revenue run rate.

**Engineering Manager - Infrastructure, Sungard Data Systems**  
October 1997 - April 1998, Mountain View, CA  
Worked on post-Infinity-acquisition efforts to bring together three product platforms.  
Continued supporting pre-acquistion Infinity customers with the same standard of service they had experienced prior to the acquisition.

**Engineering Manager - Infrastructure, Infinity Financial Technology**  
March 1995 - October 1997, Mountain View, CA  
Led a small team focused on:

- maintaining and evolving the Infinity data model on Sybase and Oracle
- maintaining an enhancing the DB access layer used by all Infinity code
- maintaining the build pipeline to produce Infinity product binaries for all supported platforms (Solaris, HP-UX, AIX)

Served as acting Director of Engineering including representing Infinity technology to investment banks involved in the [Infinity IPO](https://www.risk.net/technology/1507534/infinity-financial-technology-prepares-to-go-public-on-nasdaq).

**Manager Infrastructure, Cargill Financial Markets Division**  
April 1994 - March 1995, Minnetonka, MN  
Led a small team on these projects to align to overall industry trends:

- migration from OS/2 to Windows NT 3.5 for trader workstations
- migration from Token Ring/SNA to TCP/IP for LAN and WAN networking
- migration from AS/400 toward Unix-based application development

**Technical Manager, Prudential Home Mortage**  
January 1993 - April 1994, Minneapolis, MN  
Co-developer of a mortgage pricing engine used by telesales staff to quote realtime mortgage rates to prospects:

* serverside was built in C and multithreaded using Sybase OpenServer as a framework
* deployed this server to on premise AIX machines
* clientside was implemented in PowerBuilder allowing for easy integration with the preferred development tool used by the broader team

**Senior Software Engineer, Infinity Financial Technology**  
November 1990 - December 1992, Mountain View, CA  
First engineer hired; worked on:

- creating C++ foundation libraries
- implementing the Infinity datamodel on Sybase
- qualifying 3rd-party technologies such as portable UI toolkits
- partnering with early customers on adopting the Infinity class libraries and application suite

**Analyst, Hewlett-Packard Company**  
June 1988 - November 1990, Palo Alto, CA  
Part of the founding team of [HP SPaM](https://en.wikipedia.org/wiki/HP_SPaM) which brought supply chain modeling and analysis to HP and the industry in general.

* reported to [Corey Billington](https://www.linkedin.com/in/corey-billington-1002b71/)  
* worked with [Hau Lee](https://www.gsb.stanford.edu/faculty-research/faculty/hau-l-lee) to implement analytic models and apply this modeling tool within HP.  Hau specified the models and I implemented that design in ANSI C on HP-UX.
* I learned through this work that I enjoyed software development more than technical analysis and I subsequently turned my career in that direction

### Education

**Stanford University**  
[M.S. Management Science & Engineering](https://msande.stanford.edu/academics/graduate/masters-program/ms-program)  
April 1987 - April 1989

**Stanford University**  
[B.S. Electrical Engineering](https://ee.stanford.edu/admissions/bs)  
September 1984 - June 1988

specialization in software

### Certifications

[AWS Certified Solutions Architect - Professional](https://www.youracclaim.com/badges/f0f8e8c0-ebca-425b-ab03-2149f02b4d7f/public_url), Feb 2020 - present  

[AWS Certified Solutions Architect - Associate](https://www.youracclaim.com/badges/48079df6-a733-4c73-aa83-7a95b95b9473/public_url), May 2015 - present  

AWS Certified Developer - Associate,  June 2015 - June 2018  

AWS Certified SysOps Administrator - Associate,  August 2015 - August 2018
